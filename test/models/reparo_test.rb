require 'test_helper'

class ReparoTest < ActiveSupport::TestCase

	test "Deve mostrar todos os publicados" do
		@reparos  = Reparo.published()
		assert_not_nil @reparos
	end

  test "Deve mostrar reparos por marca" do
  	@reparos  = Reparo.showByDeviceMarca("Apple")
  	assert_not_nil @reparos
  end

  test "Deve mostrar os reparos mais vistos" do
	  @reparos = Reparo.mostViewed()
	  assert_not_nil @reparos
  end

  test "Deve mostrar reparos por tipo " do
    @reparos = Reparo.showByTipo("troca")
    assert_not_nil @reparos
  end

  test "Deve mostrar reparo com id 2" do 
  	@reparo = Reparo.showById(2)
  	assert_not_nil @reparo
  end

  test "Deve mostrar reparo com o slug" do
		#slug = "reparo-de-botao-sleep-wake-do-iphone-5" #vai retornar false, o reparo está published: false
		slug = "troca-de-display-de-samsung-galaxy"
		@reparo  = Reparo.showBySlug(slug)

		assert_not_nil @reparo
  end

  test "Deve mostrar reparos por tipo de aparelho" do
    @tablets = Reparo.showByDeviceTipo("tablets")
    @smartphones = Reparo.showByDeviceTipo("smartphones")
    assert_not_nil @smartphones
    assert_not_nil @tablets
  end

  test "Deve mostrar reparos pelo usuario " do 
  	@reparos = Reparo.fromUserId(5);
    puts @reparos
    assert_not_nil @reparos
  end
  
  test "Deve mostrar preview do reparo por id" do
    @reparos = Reparo.preview(3) #registro com id 3 nas fixtures é published false
    assert_not_nil @reparos
  end

  test "Deve salvar reparo" do 

    reparo = Reparo.new() do |r|
      r.device_id = 1
      r.admin_user_id = 1
      r.nome = "Troca de display do aparelho"
      r.descricao = "Aqui vamos fazer ums troca de display do aparelho"
      r.tipo = "troca"
      r.published = true
    end
    
    assert reparo.save()
  end

  test "Nao deve salvar reparo repetido" do
    reparo1 = Reparo.new() do |r|
      r.device_id = 1
      r.admin_user_id = 1
      r.nome = "Troca de display do aparelho"
      r.descricao = "Aqui vamos fazer ums troca de display do aparelho"
      r.tipo = "troca"
      r.published = true
    end

    reparo2 = Reparo.new() do |r|
      r.device_id = 1
      r.admin_user_id = 1
      r.nome = "Troca de display do aparelho"
      r.descricao = "Aqui vamos fazer ums troca de display do aparelho"
      r.tipo = "troca"
      r.published = true
    end

    assert reparo1.save(), "Salvou reparo unico"
    assert_not reparo2.save(), "Salvou reparo repetido"
  end

end
