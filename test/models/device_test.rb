require 'test_helper'

class DeviceTest < ActiveSupport::TestCase
	test "Deve mostrar todos os deveices publicado" do 
		@devices = Device.published
		assert_not_nil @devices
	end

	test "Deve mostrar os devices mais vistos" do
		@maisVistos = Device.mostViewed()
		assert_not_nil @maisVistos
	end
	
	test "Deve mostrar device com o id" do
		@device = Device.showById(1)
		assert_not_nil @device , @device.nil? ? "Voltou nil" : "Funegou!"
	end

	test "Deve mostrar device com slug" do
		slug = "samsung-galaxy-tab-4"
		@device = Device.showBySlug(slug)
		assert_not_nil @device, "Voltou nil"
	end

	test "Deve mostrar preview ..." do 
		assert true
	end

	test "Deve mostrar devices por tipo" do
		@smartphones = Device.showByType("smartphones")
		@tablets = Device.showByType("tablets")

		assert_not_nil @smartphones
		assert_not_nil @tablets
	end
end
