require 'test_helper'

class DevicesControllerTest < ActionController::TestCase
  setup do
    @device = devices(:one)
    @devices = devices
    @deviceFalse = devices(:four)
  end

  teardown do
    @device = nil
    @devices = nil
    @deviceFalse = nil
  end

  test "Deve mostrar index com variavel devices not nil" do
    get :index
    assert_response :success
    assert_not_nil assigns(:devices)
  end

  test "Deve mostrar device com id" do
    get :showById, id: @device
    assert_response :success
    assert_not_nil assigns(:device)
  end

  test "Deve mostar device com slug" do 
    get :showBySlug, slug: @device.slug.to_s
    assert_not_nil :success
    assert_not_nil assigns(:device)
  end

  test "Nao deve mostrar device por slug published false" do 
    get :showBySlug, slug:@deviceFalse.slug.to_s
    assert_response :missing
    assert_nil assigns(:device)
  end

  test "Nao deve mostrar device por id published false" do 
    get :showById, id: @deviceFalse
    assert_response :missing
    assert_nil assigns(:device)
  end
end
