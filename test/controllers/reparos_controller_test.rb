require 'test_helper'

class ReparosControllerTest < ActionController::TestCase
  setup do
    @reparo = reparos(:one)
    @reparos = reparos
    @reparoFalse = reparos(:three)
    @device = devices(:two)
  end
  
  teardown do
    @reparo = nil
    @reparos = nil
    @reparoFalse = nil
  end

  test "Deve mostrar o index com os reparos publicados" do
    get :index
    assert_response :success
    assert_not_nil assigns(:reparos)
  end

  test "Deve mostrar um reparo com um  ID" do
    get :showById, id: @reparo
    assert_response :success
    assert_not_nil assigns(:reparo)
  end
  
  test "Nao deve mostrar um reparo  published false por id" do
    get :showById, id: @reparoFalse
    assert_response :missing
    assert_nil assigns(:reparoFalse)
  end

  test "Nao deve mostrar repado published false por slug" do 
    get :showBySlug, slug: @reparoFalse.slug.to_s
    assert_response :missing
    assert_nil assigns(:reparoFalse)
  end

  test "Deve mosrtrar um reparo com um slug" do
    get :showBySlug, slug: @reparo.slug.to_s
    assert_response :success
    assert_not_nil assigns(:reparo), "@reparo é nil"
  end

  test "Deve mostrar reparos por marca de device" do
    get :showByDeviceMarca, marca:"Apple"
    assert_response :success
    assert_not_nil assigns(:reparos)
  end

  test "Deve mostrar reparos por tipo de device" do 
    get :showByDeviceTipo, tipo:"smartphones"
    assert_response :success
    assert_not_nil assigns(:reparos)
  end

  test "Deve mostrar reparos por tipo de reparo" do
    get :showByTipo, tipo: @reparo.tipo
    assert_response :success
    assert_not_nil assigns(:reparos)
  end

end
