namespace :db do
  desc "Rake db:drop"
  task :drop => :environment do
  	# rake db:drop
  	Rake::Task['db:drop'].invoke
  	puts "DB dropado!"
  end
  
  desc "Rake db:migrate"
  task :migrate => :environment do
  	# rake db:migrate
  	Rake::Task['db:migrate'].invoke
  	puts "DB migrado!"
  end

  desc "Rake db:seed"
  task :seed => :environment do
  	# rake db:seed
    Rake::Task['db:seed'].invoke
  	puts "DB semeado!"
  end

  desc "arruma tudo"
  task :reset => [:drop, :migrate, :seed] do
  	puts "Weeeee !!! DB arrumado!"
  end
end