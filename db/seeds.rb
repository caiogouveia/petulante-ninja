# This file shoud contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

##
#variables
##
testAssetsPath = Rails.root.to_s+"/test/Assets"


##
# Reset all the shit !
##
Device.delete_all
Reparo.delete_all
Foto.delete_all
# ActiveRecord::Base.connection.execute("TRUNCATE devices") #Mysql
ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'devices'") #sqlite
ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'reparos'") #sqlite
ActiveRecord::Base.connection.execute("DELETE from sqlite_sequence where name = 'fotos'") #sqlite



##
# Device
##



	Device.create([
		{
			nome:'iPhone 3GS',
      marca: 'Apple',
      descricao:'iPhone 3GS',
			admin_user_id: 1,
      tipo: "smartphones",
			image: File.open(testAssetsPath+"/Device/iphone3gs.jpg"),
			published: true
		},
    {
      nome:'iPhone 4',
      marca: 'Apple',
      descricao:'iPhone 4',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/iphone4.jpg"),
      published: true
    },
    {
      nome:'iPhone 4S',
      marca: 'Apple',
      descricao:'iPhone 4S',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/iphone4s.jpg"),
      published: true
    },
    {
      nome:'iPhone 5',
      marca: 'Apple',
      descricao:'iPhone 5',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/iphone5.jpg"),
      published: true
    },
    {
      nome:'iPhone 5S',
      marca: 'Apple',
      descricao:'iPhone 5S',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/iphone5s.jpg"),
      published: true
    },
    {
      nome:'iPhone 5C',
      marca: 'Apple',
      descricao:'iPhone 5C',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/iphone5c.jpg"),
      published: true
    },
    {
      nome:'Samsung S3',
      marca: 'Samsung',
      descricao:'Samsung S3',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/samsungs3.jpg"),
      published: true
    },
    {
      nome:'Samsung S4',
      marca: 'Samsung',
      descricao:'Samsung S4',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/samsungs4.jpg"),
      published: true
    },
    {
      nome:'Moto X',
      marca: 'Motorola',
      descricao:'Moto X',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/motox.jpg"),
      published: true
    },
    {
      nome:'Moto G',
      marca: 'Motorola',
      descricao:'Moto G',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/motog.jpg"),
      published: true
    },
    {
      nome:'Moto E',
      marca: 'Motorola',
      descricao:'Moto E',
      admin_user_id: 1,
      tipo: "smartphones",
      image: File.open(testAssetsPath+"/Device/motoe.jpg"),
      published: true
    },
    {
      nome:'Tablet Samsung Galaxy Tab 4',
      marca: 'Motorola',
      descricao:'Tablet da Samsung Galaxy Tab 4 com tela de X polegadas',
      admin_user_id: 1,
      tipo: "tablets",
      image: File.open(testAssetsPath+"/Device/tablet-samsung-galaxy-tab-4.jpg"),
      published: true
    }
	])


	puts "Devices done."

##
# Reparo
##
Reparo.delete_all
@descricoes = {:tipo=>"",:descricao => ""}

5.times do |i|
@device = Device.select(:id,:nome).sample
  Reparo.create(
    {
      device_id: @device.id,
      admin_user_id: 1,
      nome:"Troca de display do aparelho #{@device.nome.to_s}",
      descricao:"Aqui vamos fazer ums troca de display do aparelho #{@device.nome.to_s}",
      tipo: "troca",
      banner: File.open(Dir[testAssetsPath+"/Reparo/*"].sample.to_s),
      published: true
    }
  )
end
puts "Reparo done."

##
# Foto
##
Foto.delete_all

5.times do |i|
  Foto.create(
    {
      reparo_id: Reparo.select(:id).sample.id,
      titulo:'Foto de teste '+i.to_s,
      # descricao:'Descrição de teste...',
      image: File.open(Dir[testAssetsPath+"/Foto/*"].sample.to_s),
      # published: true
    }
  )
end
puts "Foto done."
