class AddDeviceIdToReparo < ActiveRecord::Migration
  def change
    add_column :reparos, :device_id, :integer
  end
end
