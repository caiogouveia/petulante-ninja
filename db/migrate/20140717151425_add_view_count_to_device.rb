class AddViewCountToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :view_count, :integer
  end
end
