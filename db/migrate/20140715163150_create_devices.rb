class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.string :nome
      t.text :descricao
      t.string :marca
      t.boolean :published

      t.timestamps
    end
  end
end
