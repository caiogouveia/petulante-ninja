class CreateReparos < ActiveRecord::Migration
  def change
    create_table :reparos do |t|
      t.string :nome
      t.text :descricao
      t.string :tipo
      t.boolean :published
      t.string :slug

      t.timestamps
    end
  end
end
