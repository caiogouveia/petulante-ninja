class AddAdminUserIdToDevice < ActiveRecord::Migration
  def change
    add_column :devices, :admin_user_id, :integer
  end
end
