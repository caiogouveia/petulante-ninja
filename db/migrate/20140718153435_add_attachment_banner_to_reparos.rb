class AddAttachmentBannerToReparos < ActiveRecord::Migration
  def self.up
    change_table :reparos do |t|
      t.attachment :banner
    end
  end

  def self.down
    remove_attachment :reparos, :banner
  end
end
