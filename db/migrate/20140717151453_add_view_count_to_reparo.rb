class AddViewCountToReparo < ActiveRecord::Migration
  def change
    add_column :reparos, :view_count, :integer
  end
end
