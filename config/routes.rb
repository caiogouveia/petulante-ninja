Rails.application.routes.draw do

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  resources :devices
  resources :reparos
  resources :foto

  get '/aparelhos/slug/:slug', to: 'devices#showBySlug' ,:as => :aparelhos_slug
  get '/aparelhos/id/:id', to: 'devices#showById' ,:as => :aparelhos_id
  get '/aparelhos', to: 'devices#index'

  get "/reparos/id/:id", to: "reparos#showById", :as => :reparos_id
  get "/reparos/slug/:slug", to: "reparos#showBySlug", :as => :reparo_slug
  get "/reparos/aparelhos/:tipo", to: "reparos#showByDeviceTipo", :as => :reparo_aparelho_tipo
  get "/reparos/marca/:marca", to: "reparos#showByDeviceMarca", :as => :reparo_aparelho_marca
  get "/reparos/tipo/:tipo", to: "reparos#showByTipo", :as => :reparo_tipo
  

  root 'welcomes#index'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
