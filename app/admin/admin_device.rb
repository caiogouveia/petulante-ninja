ActiveAdmin.register Device, :as => "Aparelhos" do
  menu :parent => "Aparelhos",
  :label => "Todos os Aparelhos",
  :priority => 1

##
# Config
##
  # config.clear_sidebar_sections!
  config.sort_order = "updated_at_desc"
  config.per_page = 10


##
# Permitted Parameters
##
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :nome, :marca,:tipo, :descricao,:admin_user_id, :slug, :published, :image

  #
  # oattr_reader :attr_names
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

##
# Filters
##
  filter :false
  # filter :published, as: :select, :label => "Artigo Publicado", collection: [["Publicado", true], ["Não Publicado", false]]
  # filter :admin_user_id, :as => :check_boxes, :collection => proc { AdminUser.all }


##
# Controller
# Overwrite Controller behavior Stuff
##

  controller do
    def create
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end

    def update
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end
  end

##
# Index action
##
  index :as => :grid, :columns => 4 do |device|
      # span device.nome
      image_tag(device.image.url(:thumb))
      link_to(image_tag(device.image.url(:thumb)),edit_admin_aparelho_path(device))
  end

##
# Show action
##

# show do |post|
#       attributes_table do
#         row :imagem do
#           image_tag(post.imagem.url(:thumb))
#         end
#         row :title
#         row :body do
#           raw(post.body)
#         end
#       end
# end

##
# Form
##

  form :html => { :enctype => "multipart/form-data" } do |f|
     f.inputs "" do
      f.input :admin_user_id, :label => 'Usuário', :as => :select, :selected => 1, :collection => AdminUser.all.map{|u| ["#{u.email}", u.id]}
      f.input :image,:label=>"Imagem do Aparelho", :as => :file, :class=>"fileinput", :hint => f.template.image_tag(f.object.image.url(:thumb))
      f.input :nome,:label=>"Nome do Aparelho"
      f.input :marca,:label=>"Marca do Aparelho"
      # f.input :tipo,:label=>"Tipo do Aparelho", :hint => "smartphones, tablets, celulares"
      f.input :tipo, :as => :select, :selected => :smartphones, :collection => {:Celulares => "celulares",:Smartphones => "smartphones", :Tablets => "tablets"}
      f.input :descricao,:label=>"Descrição"
      f.input :published,:label=>"Publicado ?"
      # f.input :tag_list, :hint => 'Comma separated'
      f.has_many :reparo, :allow_destroy => true, :heading => "Reparos desse Aparelho", :new_record => false do |foto|
              # foto.input :image, :label=>"Foto", :as => :file, :hint => foto.template.image_tag(foto.object.image.url(:thumb))
              foto.input :nome, :label => "Título do Reparo"

      end
    end
    f.actions
   end



end
