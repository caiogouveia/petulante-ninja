ActiveAdmin.register_page "Dashboard" do

  # menu :label => proc{ I18n.t("active_admin.dashboard") },
  # :priority => 0

  menu false


  content title: proc{ I18n.t("active_admin.dashboard") } do
    div class: "blank_slate_container", id: "dashboard_default_message" do
      # span class: "blank_slate" do
        # span I18n.t("active_admin.dashboard_welcome.welcome")
        # small I18n.t("active_admin.dashboard_welcome.call_to_action")
      # end
    end

    # Here is an example of a simple dashboard with columns and panels.
    #
    columns do
      column do
        panel "Aparelhos Recentes" do
          ul do
            Device.last(5).map do |ap|
              li link_to(ap.nome, edit_admin_aparelho_path(ap))
            end
          end
        end
      end
        column do
          panel "Reparos Recentes" do
            ul do
              Reparo.last(5).map do |reparo|
                li link_to(reparo.nome, edit_admin_reparo_path(reparo))
              end
            end
          end
      end

      column do
        panel "Informação Geral" do
          para "Bem Vindo à Área Administrativa"
        end
      end
    end
  end # content
end
