ActiveAdmin.register Reparo, :as => "Reparos" do
  menu :parent => "Reparos",
  :label => "Todos os Reparos",
  :priority => 2


##
# Configs
##
  # config.clear_sidebar_sections!
  config.sort_order = "updated_at_desc"
  config.per_page = 10


##
# Permitted Parameters
##
  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :nome, :descricao, :tipo, :slug, :published, :admin_user_id, :device_id, :view_count, :banner,
  fotos_attributes: [:id, :titulo, :descricao, :image, :reparo_id, :_destroy]



  #
  # oattr_reader :attr_names
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

##
# Filters
##
  # filter :published, as: :select, :label => "Artigo Publicado", collection: [["Publicado", true], ["Não Publicado", false]]
  # filter :admin_user_id, :as => :check_boxes, :collection => proc { AdminUser.all }

  filter :false



##
# Controller
# Overwrite Controller behavior Stuff
##

  controller do
    def create
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end

    def update
      super do |format|
        redirect_to collection_url and return if resource.valid?
      end
    end
  end



##
# Index action
##
  #   index :as => :block do |reparo|
  #     div :class => "imagem", :style => "" do
  #       if !reparo.device.nil?
  #         link_to image_tag(reparo.device.image.url(:thumb)), edit_admin_reparo_path(reparo)
  #       end
  #     end
  #     div :class => "conteudo", :style => "" do
  #       span h2 link_to reparo.nome, edit_admin_reparo_path(reparo)
  #       span "Aparelho:"
  #       if !reparo.device.nil?
  #         link_to reparo.nome, edit_admin_aparelho_path(reparo.device)
  #       end
  #       # span raw(post.subtitle)
  #       # span "Categoria:"
  #       # span link_to post.category.name, edit_admin_categorium_path(post.category)
  #     end
  # end


index :as => :grid, :columns => 4 do |reparo|
    # span reparo.nome
    # image_tag(reparo.device.image.url(:thumb))
    link_to(image_tag(reparo.device.image.url(:thumb)),edit_admin_reparo_path(reparo))
end


##
# Show action
##

# show do |post|
#       attributes_table do
#         row :imagem do
#           image_tag(post.imagem.url(:thumb))
#         end
#         row :title
#         row :body do
#           raw(post.body)
#         end
#       end
# end

##
# Form
##

  form :html => { :enctype => "multipart/form-data" } do |f|
     f.inputs "" do
      f.input :banner,:label=>"Imagem do Banner", :as => :file, :hint => f.template.image_tag(f.object.banner.url(:thumb))
      f.input :admin_user_id, :label => 'Usuário', :as => :select, :selected => 1, :collection => AdminUser.all.map{|u| ["#{u.email}", u.id]}
      f.input :device_id,:label => 'Aparelho', :as => :select, :selected => f.object.device_id, :collection => Device.all.map{|device| ["#{device.nome}", device.id]}
      f.input :nome,:label=>"Nome"
      f.input :tipo,:label=>"Tipo do reparo"
      f.input :descricao,:label=>"Descrição simples do reparo"
      f.input :published,:label=>"Publicado ?"
      # f.input :tag_list, :hint => 'Comma separated'
      # f.has_many :categories, :allow_destroy => true, :heading => 'Themes', :new_record => false do |cf|

      f.has_many :fotos, :allow_destroy => true, :new_record => true, :heading => "Fotos do Reparo" do |foto|
              foto.input :image, :label=>"Foto", :as => :file, :hint => foto.template.image_tag(foto.object.image.url(:thumb))
              foto.input :titulo, :label => "Título da foto"

      end
    end
    f.actions
   end



end
