ActiveAdmin.register Foto, :as => "Fotos" do
  menu false
  # menu :parent => "Fotos",
  # :label => "Todas as Fotos",
  # :priority => 8



  # config.clear_sidebar_sections!
  config.sort_order = "updated_at_desc"
  config.per_page = 10


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  permit_params :titulo, :descricao, :published, :image, :reparo_id


  #
  # oattr_reader :attr_names
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  filter :false
  # filter :published, as: :select, :label => "Artigo Publicado", collection: [["Publicado", true], ["Não Publicado", false]]
  # filter :admin_user_id, :as => :check_boxes, :collection => proc { AdminUser.all }

  ##
  # Index action
  ##
  # index :as => :block do |foto|
  #     div :class => "imagem", :style => "" do
  #         link_to image_tag(foto.image.url(:thumb)), edit_admin_foto_path(foto)
  #     end
  #     # div :class => "conteudo", :style => "" do
  #     #   span h2 link_to post.title, edit_admin_postagen_path(post)
  #     #   span raw(post.subtitle)
  #     #   span "Categoria:"
  #     #   span link_to post.category.name, edit_admin_categorium_path(post.category)
  #     # end
  # end


  ##
  # Controller
  # Overwrite Controller behavior Stuff
  ##

    controller do
      def create
        super do |format|
          redirect_to collection_url and return if resource.valid?
        end
      end

      def update
        super do |format|
          redirect_to collection_url and return if resource.valid?
        end
      end
    end


  ##
  # Show action
  ##

  # show do |post|
  #   attributes_table do
  #     row :imagem do
  #       image_tag(post.imagem.url(:thumb))
  #     end
  #     row :title
  #     row :body do
  #       raw(post.body)
  #     end
  #   end
  # end

  ##
  # Form
  ##

  form :html => { :enctype => "multipart/form-data" } do |f|
     f.inputs "Formulário para Adicionar uma Nova Foto" do
      f.input :image,:label=>"Foto", :as => :file, :hint => f.template.image_tag(f.object.image.url(:thumb))
      f.input :reparo_id, :label => 'Qual Reparo ?', :as => :select, :collection => Reparo.all.map{|r| ["#{r.nome}", r.id]}
      f.input :titulo,:label=>"Título"
      # f.input :descricao,:label=>"Descrição"
      # f.input :published,:label=>"Publicado ?"
      # f.input :tag_list, :hint => 'Comma separated'
    end
    f.actions
   end



end
