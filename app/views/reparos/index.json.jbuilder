json.array!(@reparos) do |reparo|
  json.extract! reparo, :id, :nome, :descricao, :tipo, :published, :slug
  json.url reparo_url(reparo, format: :json)
end
