json.array!(@devices) do |device|
  json.extract! device, :id, :nome, :descricao, :marca, :slug
  json.url device_url(device, format: :json)
end
