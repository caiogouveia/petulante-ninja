class DevicesController < ApplicationController
  before_action :set_device, only: [:show, :showById]

  # GET /devices
  # GET /devices.json
  def index
    @devices = Device.published()
  end

  # GET /devices/1
  # GET /devices/1.json
  def showBySlug
      @device = Device.showBySlug(params[:slug])
      if @device.nil?
        render :file => 'public/404.html', :status => :not_found, :layout => false
      else
        render "show"
      end
  end

  # GET /devices/1
  # GET /devices/1.json
  def showById
      @device = Device.showById(params[:id])
      if @device.nil?
        render :file => 'public/404.html', :status => :not_found, :layout => false
      else
        render "show"
      end
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_device
      # @device = Device.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def device_params
      params.require(:device).permit(:nome, :descricao, :marca, :slug)
    end
end
