class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception



	# after_filter :after
	# around_filter :around
	before_filter :before

	# def around
	# end


	# def after
	# end


	def before
		I18n.locale = params[:locale] || I18n.default_locale
		# @message = Message.new
		@reparosMaisVistos = Reparo.mostViewed().last(4)
		@reparosApple = Reparo.showByDeviceMarca("Apple")
		@reparosSamsung = Reparo.showByDeviceMarca("Samsung")
	end
end
