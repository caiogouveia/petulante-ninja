class ReparosController < ApplicationController
  before_action :set_reparo, only: [:index,:show]

  # GET /reparos
  # GET /reparos.json
  def index
    @reparos = Reparo.published(params[:pagina])
  end

  def showByTipo 
    @reparos = Reparo.showByTipo(params[:tipo])
  end

  # GET /reparos/marca/apple
  # GET /reparos/marca/apple.json
  def showByDeviceMarca
    @reparos = Reparo.showByDeviceMarca(params[:marca],params[:pagina])
  end
  # GET /reparos/aparelhos/smartphones
  # GET /reparos/aparelhos/smartphones.json
  def showByDeviceTipo
    @reparos = Reparo.showByDeviceTipo(params[:tipo],params[:pagina])
    if @reparos.nil?
      render :file => 'public/404.html', :status => :not_found, :layout => false
    end
  end

  # GET /reparos/slug/reparo-de-tela-de-iphone
  # GET /reparos/slug/reparo-de-tela-de-iphone.json
  def showBySlug
    @reparo = Reparo.showBySlug(params[:slug])
    if @reparo.nil?
      render :file => 'public/404.html', :status => :not_found, :layout => false
    else
      render "show"
    end
  end

  # GET /reparos/id/1
  # GET /reparos/id/1.json
  def showById
    @reparo = Reparo.showById(params[:id])
    if @reparo.nil?
      render :file => 'public/404.html', :status => :not_found, :layout => false
    else
      render "show"
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_reparo
      # @reparo = Reparo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def reparo_params
      params.require(:reparo).permit(:nome, :descricao, :tipo, :published, :slug)
    end
end
