class Device < ActiveRecord::Base

	##
	# Bussiness Logic
	##

	# before_save: before_save
	# def before_save
	# 	self.marca.capitalize 
	# end

	##
	# Mostra os publicados mais visualizados
	##
	def self.mostViewed(pagina=1)
		@temp = where(published:true)
								.order(view_count: :desc)
								.page pagina
		return @temp
	end

	##
	# Show all Published records, ordered by update_at: :desc
	##
	def self.published(pagina=1)
		@tempPosts = where(published:true)
								.order(updated_at: :desc)
								.page pagina
		return @tempPosts
	end

	##
	# Show record with a given slug, that is there is Published
	##
	def self.showById(id)
		@temp = where(published:true).find_by id: id

		if !@temp.nil? 
			@temp.increment(:view_count, by = 1).save;
		end
		return @temp
	end

	##
	# Show record with a given slug, that is there is Published
	##
	def self.showBySlug(slug)
		@temp = where(published:true).find_by slug: slug
		if !@temp.nil? 
			@temp.increment(:view_count, by = 1).save;
		end
		return @temp
	end

	##
	# Show record with a given id, that is there there is Published
	##
	def self.preview(id)
	  	return 	@device = self.find(id)
	end

	##
	# Show devices with the given type "smartphones","tablets"
	##
	def self.showByType(type,pagina=1)
		return where(published: true, tipo: type)
				.order(updated_at: :desc)
				.page pagina
	end

	##
	# Show records by category, Published and ordered by update_at: :desc
	##
	def self.byCategory(category_id,pagina=1)
		@temp = where(published:true,category_id: category_id)
						.order(updated_at: :desc)
						.page pagina
		return @temp
	end

	##
	# Some kinda behaviors
	##

	##
	# Url slugfy stuff from stringex gem
	##
	acts_as_url :nome, :url_attribute => "slug", :sync_url => true

	##
	# tags and tags Stuff from acts-as-taggable-on gem
	##
	# acts_as_ordered_taggable
	# acts_as_ordered_taggable_on :skills, :interests



	##
	# Paperclip config
	##
	has_attached_file :image,
	:styles => {
		:bigThumb => "512x512",
		:thumb => "128x128#"
	},
	:convert_options => {
		:thumb => "-quality 75 -strip",
		:bigThumb => "-quality 80 -strip"
	},
	:default_url => "/assets/question-mark.jpg"


	##
	# Validações
	##

	# Validate file size
	validates_with AttachmentSizeValidator, :attributes => :image, :less_than => 1.megabytes, :message => "A imagem deve ter menos de um megabyte."
  # Validate content type
	validates_attachment_content_type :image, :content_type => /\Aimage/
  # Validate filename
  validates_attachment_file_name :image, :matches => [/png\Z/, /jpe?g\Z/]



	##
	# Associações
	##

	belongs_to :admin_user
	has_many :reparo
	accepts_nested_attributes_for :reparo, allow_destroy: true


	##
	# Declarações de Filtros
	##
	# before_save :create_permalink

	##
	# Private stuff
	##

	# private
	# def create_permalink
	#   self.slug = nome.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
	# end
end
