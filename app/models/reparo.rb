class Reparo < ActiveRecord::Base
  ##
  # kaminari
  ##
  paginates_per 2
  @listPerPage = 15

  ##
  # Bussiness Logic
  ##

  ##
  # mostrar os reparos por tipo de aparelho
  ##
  def self.showByDeviceTipo(type,pagina=1)
    return includes(:device)
          .where(published:true, :devices=>{tipo:type})
          .order(view_count: :desc)
          .page pagina
  end

  ##
  # Mostrar os reparos por marca do aparelho
  ##
  def self.showByDeviceMarca(marca,pagina=1)
    return    includes(:device)
              .where(published:true,:devices => {marca: marca.capitalize})
              .order(view_count: :desc)
              .page pagina
  end

  ##
  # Mostrar os mais visualizados
  ##
  def self.mostViewed(pagina=1)
    @temp = where(published:true)
                .order(view_count: :desc)
                .page pagina
    return @temp
  end

  ##
  # Show all records marked as Published, ordered by update_at: :desc
  ##
  def self.published(pagina=1)
    @temp = where(published:true)
                .order(updated_at: :desc)
                .page pagina
    return @temp
  end

  ##
  # Show record with a given slug, that is there is Published
  ##
  def self.showById(id)
    @temp = where(published:true).find_by id: id
    
    if !@temp.nil?
      @temp.increment(:view_count, by = 1).save
    end

    return @temp
  end

  ##
  # Show record with a given slug, that is there is Published
  ##
  def self.showBySlug(slug)
    @temp = where(published:true)
            .find_by slug: slug
            
            if !@temp.nil? 
              @temp
              .increment(:view_count, by = 1)
              .save;
            else
            end
    return @temp
  end

  ##
  # Show record with a given id, that is there there is Published
  ##
  def self.preview(id)
    return find(id)
  end


  ##
  # Show records from a user, Published and ordered by update_at: :desc
  ##
  def self.fromUserId(user_id,pagina=1)
    @temp = where(published:true,admin_user_id: user_id)
                .order(updated_at: :desc)
                .page pagina
    return @temp
  end

  ##
  # Show records by Reparo Tipo
  ##
  def self.showByTipo(tipo,pagina=1)
    return where(published:true,tipo: tipo).order(updated_at: :desc).page pagina
  end


  ##
  # Some kinda behaviors
  ##

  ##
  # Paperclip config
  ##
  has_attached_file :banner,
  :styles => {
    :fullHd => "1920x360",
    :thumb => "640x360"
  },
  :convert_options => {
    :thumb => "-quality 75 -strip",
    :bigThumb => "-quality 80 -strip"
  },
  :default_url => "/assets/banner-question-mark.jpg"

  ##
  # Url slugfy stuff from stringex gem
  ##
  acts_as_url :nome, :url_attribute => "slug", :sync_url => true


  ##
  # tags and tags Stuff from acts-as-taggable-on gem
  ##
  #TODO: ver como funciona essas TAGS, ver no github do projeto
  # acts_as_ordered_taggable
  # acts_as_ordered_taggable_on :skills, :interests

  ##
  # Validações
  ##
  validates :nome, presence: true, uniqueness: true
  validates :tipo, presence: true
  validates :admin_user_id, presence: true

  # Validate file size
  #TODO: especificar um tamanho melhor de imagem para ser uplodeada como um banner FullHD
  validates_with AttachmentSizeValidator, :attributes => :banner, :less_than => 1.megabytes, :message => "A imagem deve ter menos de um megabyte."
  # Validate content type
  validates_attachment_content_type :banner, :content_type => /\Aimage/
  # Validate filename
  validates_attachment_file_name :banner, :matches => [/png\Z/, /jpe?g\Z/]


  ##
  # Associações
  ##
  belongs_to :device
  belongs_to :admin_user
  has_many :fotos
  accepts_nested_attributes_for :fotos, allow_destroy: true

end
