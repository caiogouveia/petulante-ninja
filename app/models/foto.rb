class Foto < ActiveRecord::Base
  ##
	# Bussiness Logic
	##

  ##
	# Show all Published photos, ordered by update_at: :desc
	##
	def self.published(pagina=1)
		@tmp = where(published:true)
								.order(updated_at: :desc)
								.page pagina
		return @tmp
	end

	##
	# Show Published photo with the given slug.
	##
	def self.slug(slug)
		@temp = where(published:true)
						.find_by slug: slug
		return @temp
	end

	##
	# Paperclip config
	##
	has_attached_file :image,
	:styles => {
		:banner => "1024x768",
		:normal => "256x256",
		:thumb => "128x128#"
	},
	:convert_options => {
		:thumb => "-quality 75 -strip",
		:normal => "-quality 80 -strip",
		:banner => "-quality 80 -strip"
	},
	:default_url => "/images/question-mark.jpg"

	##
	# Validações
	##

	# Validate file size
	validates_with AttachmentSizeValidator, :attributes => :image, :less_than => 3.megabytes, :message => "A imagem deve ter menos de um megabyte."
	# Validate content type
	validates_attachment_content_type :image, :content_type => /\Aimage/
	# Validate filename
	validates_attachment_file_name :image, :matches => [/png\Z/, /jpe?g\Z/]

	##
	# Associações
	##

	belongs_to :reparo


##
#
##
  ##
  # Declarações de Filtros
  ##
  # before_save :create_permalink


  ##
	# Private stuff
	##
	private

	def create_permalink
	  # self.slug = title.downcase.strip.gsub(' ', '-').gsub(/[^\w-]/, '')
	end
end
